const path = require('path');
const fs = require('fs');
const express = require('express');
const serveStatic = require('serve-static');
const { DateTime, Settings: LuxonSettings } = require('luxon');

const dbPath = './washed-at.txt';
const MAX_RECORDS = 10;

LuxonSettings.defaultLocale = 'ru';
LuxonSettings.defaultZoneName = 'Asia/Novosibirsk';

const apiApp = express.Router()

function getDirtiness(currentTime, lastWashed) {
    if (lastWashed.plus({ days: 1 }) >= currentTime) {
        return 'clean';
    }

    if (lastWashed.plus({ days: 2 }) >= currentTime) {
        return 'normal';
    }

    return 'dirty';
}

apiApp.get('/last-washed', (req, res) => {
    const lastWashedString = fs.readFileSync(dbPath, { encoding: 'utf-8' })
        .split('\n')[0];

    const lastWashed = DateTime.fromISO(lastWashedString);
    const now = DateTime.now();

    res.json({
        dateToday: now.toFormat('dd MMMM'),
        lastWashed: lastWashed.toFormat('dd MMMM в HH:mm'),
        dirtiness: getDirtiness(now, lastWashed),
    });
});

apiApp.post('/wash', (req, res) => {
    const washedAtEntries = fs.readFileSync(dbPath, { encoding: 'utf-8' }).split('\n');

    washedAtEntries.unshift(DateTime.utc().toISO());
    washedAtEntries.length = Math.min(washedAtEntries.length, MAX_RECORDS);

    fs.writeFileSync(dbPath, washedAtEntries.join('\n'));
    res.end();
});

const app = express();

app.use('/api', apiApp);
app.use('/assets/', express.static(path.join(__dirname, '../assets')));
app.use(express.static(path.join(__dirname, '../static')));

app.listen(3000, () => console.log('Started!'));
